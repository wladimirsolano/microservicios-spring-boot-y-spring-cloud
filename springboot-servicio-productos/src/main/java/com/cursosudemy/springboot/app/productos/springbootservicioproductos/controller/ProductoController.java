package com.cursosudemy.springboot.app.productos.springbootservicioproductos.controller;

import com.cursosudemy.springboot.app.productos.springbootservicioproductos.entity.Producto;
import com.cursosudemy.springboot.app.productos.springbootservicioproductos.service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    private IProductoService iProductoService;

    @GetMapping("/listar")
    public List<Producto> listarProductos () {
        return iProductoService.findAll();
    }

    @GetMapping("/detalle/{id}")
    public Producto detalleProducto (@PathVariable Long id) {
        return iProductoService.findById(id);
    }
}
