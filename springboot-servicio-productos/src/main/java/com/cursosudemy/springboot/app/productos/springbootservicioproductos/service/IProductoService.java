package com.cursosudemy.springboot.app.productos.springbootservicioproductos.service;

import com.cursosudemy.springboot.app.productos.springbootservicioproductos.entity.Producto;

import java.util.List;

public interface IProductoService {

    public List<Producto> findAll();
    public Producto findById (Long id);
}
