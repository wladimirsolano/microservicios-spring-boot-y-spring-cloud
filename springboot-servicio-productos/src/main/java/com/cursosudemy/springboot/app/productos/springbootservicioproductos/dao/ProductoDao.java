package com.cursosudemy.springboot.app.productos.springbootservicioproductos.dao;

import com.cursosudemy.springboot.app.productos.springbootservicioproductos.entity.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDao extends CrudRepository<Producto, Long> {
}
